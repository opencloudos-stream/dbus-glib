Summary: GLib bindings for D-Bus
Name:    dbus-glib
Version: 0.112
Release: 6%{?dist}

License: AFL and GPLv2+
URL:     https://www.freedesktop.org/software/dbus/
Source0: https://dbus.freedesktop.org/releases/dbus-glib/%{name}-%{version}.tar.gz
Source1: https://dbus.freedesktop.org/releases/dbus-glib/%{name}-%{version}.tar.gz.asc

BuildRequires: pkgconfig(dbus-1) >= 1.8, dbus-daemon
BuildRequires: pkgconfig(glib-2.0) >= 2.40.0
BuildRequires: expat-devel >= 1.95.5, gettext, gcc
BuildRequires: /usr/bin/chrpath

%description
dbus-glib is a deprecated API for use of D-Bus from GLib applications.

%package devel
Summary: Libraries and headers for the D-Bus GLib bindings
Requires: %{name} = %{version}-%{release}

%description devel
This package provides Headers and static libraries for developers of D-Bus Glib.

%prep
%autosetup -p1

%build
%configure --enable-tests --enable-asserts --disable-gtk-doc
%make_build

%check
%make_build check


%install
%make_install

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la

chrpath --delete %{buildroot}%{_bindir}/dbus-binding-tool
chrpath --delete %{buildroot}%{_libexecdir}/dbus-bash-completion-helper


%files
%license COPYING
%doc NEWS
%{_libdir}/libdbus-glib-1.so.*
%{_bindir}/dbus-binding-tool
%{_mandir}/man1/dbus-binding-tool.1*

%files devel
%{_libdir}/libdbus-glib-1.so
%{_libdir}/pkgconfig/dbus-glib-1.pc
%{_includedir}/dbus-1.0/dbus/*
%{_datadir}/gtk-doc/html/dbus-glib
%attr(644, root, root) /%{_sysconfdir}/bash_completion.d/dbus-bash-completion.sh
%{_libexecdir}/dbus-bash-completion-helper


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.112-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.112-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.112-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.112-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.112-2
- Rebuilt for OpenCloudOS Stream 23

* Tue May 10 2022 cunshunxia <cunshunxia@tencent.com> - 0.112-1
- initial build
